<?php

/**
 * @file
 * uw_ct_home_page_banner.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function uw_ct_home_page_banner_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'home_page_banner_wide';
  $context->description = 'Displays a wide banner on the home page above the menus and content.';
  $context->tag = 'Content';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        '<front>' => '<front>',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'uw_ct_home_page_banner-banner' => array(
          'module' => 'uw_ct_home_page_banner',
          'delta' => 'banner',
          'region' => 'banner_alt',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 1;

  // Translatables
  // Included for use with string extractors like potx.
  t('Content');
  t('Displays a wide banner on the home page above the menus and content.');
  $export['home_page_banner_wide'] = $context;

  return $export;
}
