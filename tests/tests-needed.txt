@todo

These tests cannot be done with Simpletest

Scenario: The home page displays a random banner with JavaScript
Given that I am looking at the page regardless of login status
And JavaScript is on
When I go to the home page
The default banner should be hidden, and a random banner should be loaded using JavaScript
And it should randomize even if behind Varnish
And this process should not be painfully slow, even if the user is off campus.

For sites that are behind Varnish:
When JavaScript is off and logged-in, banner is random.
When JavaScript is off and not logged-in, banner does not change until Varnish cache expires or is emptied.
