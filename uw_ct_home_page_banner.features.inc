<?php

/**
 * @file
 * uw_ct_home_page_banner.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function uw_ct_home_page_banner_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "context" && $api == "context") {
    return array("version" => "3");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function uw_ct_home_page_banner_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_image_default_styles().
 */
function uw_ct_home_page_banner_image_default_styles() {
  $styles = array();

  // Exported image style: banner-750w.
  $styles['banner-750w'] = array(
    'label' => 'banner-750w',
    'effects' => array(
      1 => array(
        'name' => 'image_scale',
        'data' => array(
          'width' => 750,
          'height' => '',
          'upscale' => 0,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: banner-wide.
  $styles['banner-wide'] = array(
    'label' => 'banner-wide',
    'effects' => array(
      2 => array(
        'name' => 'image_scale',
        'data' => array(
          'width' => 1000,
          'height' => '',
          'upscale' => 1,
        ),
        'weight' => 1,
      ),
    ),
  );

  return $styles;
}

/**
 * Implements hook_node_info().
 */
function uw_ct_home_page_banner_node_info() {
  $items = array(
    'uw_home_page_banner' => array(
      'name' => t('Home page banner'),
      'base' => 'node_content',
      'description' => t('A home page banner photo, with caption and optional link. There can be multiple banners that change at refresh or are displayed as a slideshow.'),
      'has_title' => '1',
      'title_label' => t('Title (required, internal use only)'),
      'help' => t('A minimum banner size of 750 x 300 is recommended.'),
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
