<?php

/**
 *
 */
class RestfulBannersResource extends RestfulEntityBaseNode {

  /**
   *
   */
  public function publicFieldsInfo() {
    // Includes nid, and name (as label)
    $public_fields = parent::publicFieldsInfo();

    $public_fields['image'] = array(
      'property' => 'field_banner_image',
      'process_callbacks' => array(array($this, 'formatImage')),
    );

    $public_fields['caption'] = array(
      'property' => 'field_caption',
    );

    $public_fields['last_modified'] = array(
      'property' => 'changed',
      'process_callbacks' => array(array($this, 'formatTimestamp')),
    );

    $public_fields['link'] = array(
      'property' => 'url',
    );

    return $public_fields;
  }

  /**
   *
   */
  protected function formatImage($image) {
    return file_create_url($image['uri']);
  }

  /**
   *
   */
  protected function formatTimestamp($timestamp) {
    return date('c', $timestamp);
  }

}
