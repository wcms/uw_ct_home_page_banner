<?php

/**
 * @file
 * Creates a restful endpoint at path api/banners with uw_home_page_banner.
 */

$plugin = array(
  'label' => t('Banners'),
  'resource' => 'banners',
  'name' => 'banners',
  'entity_type' => 'node',
  'bundle' => 'uw_home_page_banner',
  'description' => t('Exports the "Home Page Banner" entity.'),
  'class' => 'RestfulBannersResource',
  'discoverable' => FALSE,
);
