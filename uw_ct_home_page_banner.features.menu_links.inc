<?php

/**
 * @file
 * uw_ct_home_page_banner.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function uw_ct_home_page_banner_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: menu-site-management_home-page-banner-settings:admin/config/system/uw_ct_home_page_banner_settings.
  $menu_links['menu-site-management_home-page-banner-settings:admin/config/system/uw_ct_home_page_banner_settings'] = array(
    'menu_name' => 'menu-site-management',
    'link_path' => 'admin/config/system/uw_ct_home_page_banner_settings',
    'router_path' => 'admin/config/system/uw_ct_home_page_banner_settings',
    'link_title' => 'Home page banner settings',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-site-management_home-page-banner-settings:admin/config/system/uw_ct_home_page_banner_settings',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 0,
    'language' => 'und',
    'menu_links_customized' => 0,
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Home page banner settings');

  return $menu_links;
}
