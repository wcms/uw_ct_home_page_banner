<?php

/**
 * @file
 * uw_ct_home_page_banner.slideshow.inc
 */

/**
 * Implements hook_permission().
 */
function uw_ct_home_page_banner_permission() {
  return array(
    'administer home page banner configuration' => array(
      'title' => t('Administer home page banner configuration'),
    ),
  );
}

/**
 * Implements hook_menu().
 */
function uw_ct_home_page_banner_menu() {
  $items['admin/config/system/uw_ct_home_page_banner_settings'] = array(
    'title' => 'Home Page Banner Settings',
    'description' => 'Configure whether or not to use slideshows, slideshow animation settings, and the banner location.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('uw_ct_home_page_banner_settings_form'),
    'access arguments' => array('administer home page banner configuration'),
  );
  $items['ajax/banner'] = array(
    'page callback' => 'uw_ct_home_page_banner_ajax',
    'access callback' => TRUE,
    'type' => MENU_CALLBACK,
  );
  return $items;
}

/**
 * Menu callback; Config form.
 */
function uw_ct_home_page_banner_settings_form($form, &$form_state) {
  $form['uw_banner_slideshow_enable'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable banner slideshows in place of normal banners'),
    '#default_value' => variable_get('uw_banner_slideshow_enable', 0),
    '#description' => t('Controls whether or not the slideshow is used.'),
  );
  $form['uw_banner_slideshow_max'] = array(
    '#type' => 'textfield',
    '#title' => t('Slideshow banners'),
    '#default_value' => variable_get('uw_banner_slideshow_max', 8),
    '#description' => t('Enter the maximum number of banners to display in a slideshow, from 1 to 8. Any additional banners will be ignored. If fewer banners than this number exist, all banners will be displayed.'),
    '#size' => 2,
    '#required' => TRUE,
  );
  $form['uw_banner_slideshow_slidespeed'] = array(
    '#type' => 'textfield',
    '#title' => t('Slide speed'),
    '#default_value' => variable_get('uw_banner_slideshow_slidespeed', 7000),
    '#description' => t('Enter the time, in milliseconds, that should elapse between slides when the slideshow is playing. Minimum 5000ms.'),
    '#size' => 5,
    '#field_suffix' => 'ms',
    '#required' => TRUE,
  );
  $form['uw_banner_slideshow_transitionspeed'] = array(
    '#type' => 'textfield',
    '#title' => t('Transition speed'),
    '#default_value' => variable_get('uw_banner_slideshow_transitionspeed', 400),
    '#description' => t('Enter the time, in milliseconds, the banner transition should take to complete. Minimum 400ms.'),
    '#size' => 5,
    '#field_suffix' => 'ms',
    '#required' => TRUE,
  );
  $form['uw_banner_slideshow_autoplay'] = array(
    '#type' => 'checkbox',
    '#title' => t('Autoplay'),
    '#default_value' => variable_get('uw_banner_slideshow_autoplay', 1),
    '#description' => t('Controls whether or not the slideshow starts immediately on page load.'),
  );
  return system_settings_form($form);
}

/**
 *
 */
function uw_ct_home_page_banner_settings_form_validate(&$form, &$form_state) {
  if (!is_numeric($form_state['values']['uw_banner_slideshow_max']) || $form_state['values']['uw_banner_slideshow_max'] < 1 || $form_state['values']['uw_banner_slideshow_max'] > 8) {
    form_set_error('uw_banner_slideshow_max', t('The number of banners must be a number between 1 and 8.'));
  }
  if (!is_numeric($form_state['values']['uw_banner_slideshow_slidespeed']) || $form_state['values']['uw_banner_slideshow_slidespeed'] < 5000) {
    form_set_error('uw_banner_slideshow_slidespeed', t('The slide speed must be a number greater than or equal to 5000.'));
  }
  if (!is_numeric($form_state['values']['uw_banner_slideshow_transitionspeed']) || $form_state['values']['uw_banner_slideshow_transitionspeed'] < 400) {
    form_set_error('uw_banner_slideshow_transitionspeed', t('The transition speed must be a number greater than or equal to 400.'));
  }
  if (is_numeric($form_state['values']['uw_banner_slideshow_slidespeed']) && is_numeric($form_state['values']['uw_banner_slideshow_transitionspeed']) && $form_state['values']['uw_banner_slideshow_slidespeed'] <= $form_state['values']['uw_banner_slideshow_transitionspeed']) {
    form_set_error('uw_banner_slideshow_slidespeed', t('The slide speed must be greater than the transition speed.'));
  }
}

/**
 * Menu callback; Handle AJAX requests for banners.
 */
function uw_ct_home_page_banner_ajax() {
  print uw_ct_home_page_banner_return();
}

/**
 *
 */
function uw_ct_home_page_banner_return() {
  if (isset($_REQUEST['uwb']) && $_REQUEST['uwb'] == 'rand') {
    // Select and fetch one random banner.
    $result = db_query_range("SELECT node.nid FROM {node} WHERE node.type = 'uw_home_page_banner' AND node.status = 1 ORDER BY RAND()", 0, 1)->fetch();

    if (isset($result->nid)) {
      $_REQUEST['uwb'] = $result->nid;
    }
  }
  if (isset($_REQUEST['uwb']) && is_numeric($_REQUEST['uwb'])) {
    // Select and fetch the specific banner.
    $result = db_query("SELECT n.nid FROM {node} n LEFT JOIN {draggableviews_structure} dv ON n.nid = dv.entity_id WHERE n.type = 'uw_home_page_banner' AND n.status = 1 AND n.nid = :nid ORDER BY dv.weight", array(':nid' => $_REQUEST['uwb']))->fetch();
    if (isset($result->nid)) {
      $node = node_load($result->nid);
      $block['subject'] = NULL;
      $block['content'] = node_view($node);
    }
  }
  if (!isset($block)) {
    // Select and fetch the first banner.
    $result = db_query("SELECT n.nid FROM {node} n LEFT JOIN {draggableviews_structure} dv ON n.nid = dv.entity_id WHERE n.type = 'uw_home_page_banner' AND n.status = 1 ORDER BY dv.weight")->fetch();
    if (isset($result->nid)) {
      $node = node_load($result->nid);
      $block['subject'] = NULL;
      $block['content'] = node_view($node);
    }
  }
  return render($block['content']);
}
