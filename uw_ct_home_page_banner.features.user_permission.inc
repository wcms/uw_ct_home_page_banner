<?php

/**
 * @file
 * uw_ct_home_page_banner.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function uw_ct_home_page_banner_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'administer home page banner configuration'.
  $permissions['administer home page banner configuration'] = array(
    'name' => 'administer home page banner configuration',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'uw_ct_home_page_banner',
  );

  // Exported permission: 'create uw_home_page_banner content'.
  $permissions['create uw_home_page_banner content'] = array(
    'name' => 'create uw_home_page_banner content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete any uw_home_page_banner content'.
  $permissions['delete any uw_home_page_banner content'] = array(
    'name' => 'delete any uw_home_page_banner content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete own uw_home_page_banner content'.
  $permissions['delete own uw_home_page_banner content'] = array(
    'name' => 'delete own uw_home_page_banner content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit any uw_home_page_banner content'.
  $permissions['edit any uw_home_page_banner content'] = array(
    'name' => 'edit any uw_home_page_banner content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit own uw_home_page_banner content'.
  $permissions['edit own uw_home_page_banner content'] = array(
    'name' => 'edit own uw_home_page_banner content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'enter uw_home_page_banner revision log entry'.
  $permissions['enter uw_home_page_banner revision log entry'] = array(
    'name' => 'enter uw_home_page_banner revision log entry',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_home_page_banner authored by option'.
  $permissions['override uw_home_page_banner authored by option'] = array(
    'name' => 'override uw_home_page_banner authored by option',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_home_page_banner authored on option'.
  $permissions['override uw_home_page_banner authored on option'] = array(
    'name' => 'override uw_home_page_banner authored on option',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_home_page_banner promote to front page option'.
  $permissions['override uw_home_page_banner promote to front page option'] = array(
    'name' => 'override uw_home_page_banner promote to front page option',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_home_page_banner published option'.
  $permissions['override uw_home_page_banner published option'] = array(
    'name' => 'override uw_home_page_banner published option',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_home_page_banner revision option'.
  $permissions['override uw_home_page_banner revision option'] = array(
    'name' => 'override uw_home_page_banner revision option',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_home_page_banner sticky option'.
  $permissions['override uw_home_page_banner sticky option'] = array(
    'name' => 'override uw_home_page_banner sticky option',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'override_node_options',
  );

  return $permissions;
}
