/**
 * @file
 */

(function ($) {
  $(function () {
    // Add the progress bar (which loads hidden).
    $('#uwb_paginator').after('<div id="uwb_progress"></div>');
    // If there is a currently selected paginator control, the preloaded banner belongs to that "page".
    $selected = $('#uwb_paginator a.uwb_selected');
    if ($selected.length) {
      url = $selected.attr('href').split('?');
      if (url[1] !== undefined) {
        // Mark the preloaded banner as belonging to this "page".
        $('#uwb_area .banner').attr('data-page',url[1]);
      }
    }

    // Start animation if neccessary.
    if (Drupal.settings.uw_banner_slideshow.autoplay) {
      $('#uwb_paginator').prepend('<li class="uwb_controls"><button class="pause" title="Pause"><span class="element-invisible">Pause banner slideshow</span><span class="icon-pause"></span></button></li>');
      window.uwb_animate = setTimeout(nextslide_uwb,Drupal.settings.uw_banner_slideshow.slidespeed);
    }
    else {
      $('#uwb_paginator').prepend('<li class="uwb_controls"><button href="#play" class="play" title="Play"><span class="element-invisible">Play banner slideshow</span><span class="icon-play"></span></button></li>');
    }
    // Handle clicks on the paginator.
    $('#uwb_paginator a, #uwb_paginator button').click(function () {
      if ($(this).hasClass('pause')) {
        // Stop the animation cycle.
        clearTimeout(window.uwb_animate);
        $(this).removeClass('pause').addClass('play').attr('title','Play').attr('href','#play').find('.element-invisible').html('Play banner slideshow').end().find('span.icon-pause').removeClass('icon-pause').addClass('icon-play');
      }
      else if ($(this).hasClass('play')) {
        // Start the animation cycle again.
        nextslide_uwb();
        $(this).removeClass('play').addClass('pause').attr('title','Pause').attr('href','#pause').find('.element-invisible').html('Pause banner slideshow').end().find('span.icon-play').removeClass('icon-play').addClass('icon-pause');
      }
      else {
        $('#uwb_paginator button.pause').click();
        if (!$(this).hasClass('uwb_selected')) {
          retreive_uwb($(this).attr('href'));
        }
      }
      return false;
    });
    // Pause the slideshow if the banner info button is clicked.
    $('#uwb_area').on('click', '.banner-caption-check', function () {
      $('#uwb_paginator button.pause').click();
    })

    // Function used to animate the transition between two banners.
    function animate_uwb() {
      // Make sure the caption is closed.
      $('.banner-caption-check').prop('checked', false);
      // Do the transition.
      $('#uwb_area .banner:not(.element-invisible)').hide().fadeIn(parseInt(Drupal.settings.uw_banner_slideshow.transitionspeed));
    }

    function retreive_uwb(url) {
      url = url.split('?');
      if (url[1] !== undefined) {
        $searched = $('#uwb_area .banner[data-page="' + url[1] + '"]');
        if ($searched.length) {
          // Banner is already loaded.
          $('#uwb_paginator a').removeClass('uwb_selected');
          $('#uwb_paginator a[href$="' + url[1] + '"]').addClass('uwb_selected');
          $('.banner').addClass('element-invisible');
          $('.banner[data-page$="' + url[1] + '"]').removeClass('element-invisible');
          animate_uwb();
        }
        else {
          // Banner must be retrieved via AJAX.
          path = Drupal.settings.basePath + 'ajax/banner/?' + url[1];
          $('#uwb_progress').show();
          // Create a placeholder DIV so nothing triggers this loading again.
          lastheight = $('#uwb_area .banner:not(.element-invisible)').height();
          $('uwb_area').prepend('<div class="banner" data-page="' + url[1] + '"></div>').height(lastheight);
          $.get(path,function (data) {
            // Remove the placeholder div.
            $('.banner[data-page$="' + url[1] + '"]').remove();
            $('#uwb_progress').hide();
            $('#uwb_area').prepend(data);
            $('#uwb_area .banner:first').attr('data-page',url[1]);
            $('#uwb_paginator a').removeClass('uwb_selected');
            $('#uwb_paginator a[href$="' + url[1] + '"]').addClass('uwb_selected');
            $('.banner').addClass('element-invisible');
            $newbanner = $('.banner[data-page$="' + url[1] + '"]').removeClass('element-invisible');
            $newbanner.height(lastheight);
            $('img', $newbanner).on('load', function () {
              $newbanner.height('auto');
            })
            animate_uwb();
          }).error(function () {
            // AJAX isn't working, fall back to old school behaviour.
            $('#uwb_paginator button.pause').click().remove();
            $('#uwb_progress').hide();
            $('#uwb_paginator a, #uwb_paginator button').unbind();
          });
        }
      }
    }

    function nextslide_uwb() {
      // Check that we have more than one banner.
      // Check the paginator to see if we have more than 1 page block.
      if ($("#uwb_paginator li:not('.uwb_controls')").length > 1) {
        // Determine what slide we are on.
        $currentslide = $('#uwb_paginator a.uwb_selected');
        // Get the next slide (if we know what slide we are on).
        if ($currentslide.length) {
          $nextslide = $currentslide.parent().next().find('a');
        }
        // If there is not a next slide (because we are at the end), or a current slide (because an older slide that is no longer in the list was bookmarked), the next slide is the first slide.
        if (!$nextslide.length || !$currentslide.length) {
          $nextslide = $('#uwb_paginator li:nth-child(2) a');
        }
        retreive_uwb($nextslide.attr('href'));
        // Start the animation cycle again.
        window.uwb_animate = setTimeout(nextslide_uwb, Drupal.settings.uw_banner_slideshow.slidespeed);
      }
    }

  });

})(jQuery);
