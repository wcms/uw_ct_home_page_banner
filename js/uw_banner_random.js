/**
 * @file
 */

// Shim to make sure Date.now works in all browsers.
if (!Date.now) {
  Date.now = function () { return new Date().getTime(); }
}

(function ($) {
  Drupal.behaviors.uw_ct_home_page_banner = {
    attach: function () {
      // Load a random banner, seeded with the current time so caching doesn't serve everyone the same banner.
      // We don't need to check if the page didn't load, because the original banner will still be there if it didn't.
      $('#random-banner').once('uw_ct_home_page_banner').load(Drupal.settings.basePath + 'ajax/banner/?uwb=rand&now=' + Date.now(), function () {
        $(this).show();
      });
    }
  };
})(jQuery);
